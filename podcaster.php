#!/usr/bin/env php
<?php
//require_once('vendor/autoload.php');
require_once('vendor/dg/rss-php/src/Feed.php');
require_once('podcasts.conf');
global $PODCASTS, $DIR;
$LAME32 = '/home/allegre/bin/lame32';


// php podcaster.php MSci  20190318 20190322

if ( isset($PODCASTS[$argv[1]]) ) {
	$url = $PODCASTS[$argv[1]];
} else {
	print_r($PODCASTS);
	die("Arg1 doit être une des clés. $argv[1] fourni.\n\n");
}

if ( isset($argv[2]) && isset($argv[3]) ) {
    $timerange = get_time_range($argv[2], $argv[3]);
}
else {
    $timerange = null;
}

$rss = Feed::loadRss($url);

define('MINSIZE', 10000000);

echo 'Title: ', $rss->title;
echo 'Description: ', $rss->description;
echo 'Link: ', $rss->link;

foreach ($rss->item as $item) {

    if (! check_time_range($timerange, $item) ) { continue ; }
	if ($item->enclosure->attributes()['length'] < MINSIZE) {
	    echo "{$item->title} out of size range.\n";
		continue;
	}
	echo 'Title: ', $item->title . "\n";
	echo 'Link: ', $item->link . "\n";
	echo 'Timestamp: ', $item->timestamp . "\n";
	echo 'Duration: ', $item->{'itunes:duration'} . "\n";
	echo 'Description ', $item->description . "\n";
	echo 'HTML encoded content: ', $item->{'content:encoded'} . "\n";
	echo "Enclosure: ", $item->enclosure->url . "\n";
	foreach($item->enclosure->attributes() as $key => $value) {
		echo "$key : $value\n";
	}
	echo "\n\n";

	$url = $item->enclosure->attributes()['url'];

	$res = exec("wget --directory-prefix=$DIR  $url", $output, $diag);  // curl -O ne marche pas ?!
    
    $pid = pcntl_fork();
    if ($pid == -1) {
        die('duplication impossible');
    } else if ($pid) { // père
         echo "Next.\n\n";  // pcntl_wait($status); // Protège encore des enfants zombies
    } else {
         reencode(get_filename($url));
    }

	echo "\nTéléchargement terminé dans : $DIR \n\n";

}

echo "Fini.";
return 0;


function get_filename($url) {
global $DIR;
    $upath = parse_url($url)['path'];
    return $DIR . '/' . basename($upath);
}


function get_time_range($begin, $end) {
    $btime = strtotime($begin . 'T00:00:00');
    $etime = strtotime($end . 'T23:59:59'); 
    return [$btime, $etime];
}


function check_time_range($timerange, $item) {
    if (! $timerange) {
        return true;
    }
    if ( $item->timestamp < $timerange[0] || $item->timestamp > $timerange[1]) {
	    echo "{$item->title} out of date range.\n";
		return false;
	}
    return true;
}


function reencode($filename) {
    global $LAME32;
    echo "reencoding $filename... ";
    pcntl_exec($LAME32, ['-d', $filename]);
    echo " $filename OK.\n";
}


